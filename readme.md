# Nearest Location API
## A demonstration app to find the nearest locations to a given latitude and longitude

### Description
This app is based on CodeIgniter. If you have not previously used this framework, please read below for intallation instructions. The .htaccess file in the root directory has a rewrite so that you don't have to include index.php in the url.

Database configuration is in application->config->database.php. The database export is in the root directory and includes the

Once apache is configured, access to the API can be made through a GET request:
   http://[base_name]/api/location?latitude=[latitude]&longitude=[longitude]
The response is a Json object.


### Improvements
* Better search algorithm - at the moment, a square is 'drawn' around the origin given and locations are searched for in that square. If no locations are found, the search area is increased. If too many locations are found, the search area is decreased. It would be better though if the search area was more closely approximated to a circle, as the current method can lead to finding a location within the square, at the corner, say, where another location just outside the square is actually closer. However, I felt the benefits of this being a more extensible approach for when more locations are added, temporarily outweighed these edge cases.
* Add unit tests
* Relocate Location_Data_Service::search_for_locations() method to another class for better separation of concerns.
* Add access authorization etc
* Test performance with scaling


###################
What is CodeIgniter
###################

CodeIgniter is an Application Development Framework - a toolkit - for people
who build web sites using PHP. Its goal is to enable you to develop projects
much faster than you could if you were writing code from scratch, by providing
a rich set of libraries for commonly needed tasks, as well as a simple
interface and logical structure to access these libraries. CodeIgniter lets
you creatively focus on your project by minimizing the amount of code needed
for a given task.

*******************
Release Information
*******************

This repo contains in-development code for future releases. To download the
latest stable release please visit the `CodeIgniter Downloads
<https://codeigniter.com/download>`_ page.

**************************
Changelog and New Features
**************************

You can find a list of all changes for each release in the `user
guide change log <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/changelog.rst>`_.

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

Please see the `installation section <https://codeigniter.com/user_guide/installation/index.html>`_
of the CodeIgniter User Guide.

*******
License
*******

Please see the `license
agreement <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_.

*********
Resources
*********

-  `User Guide <https://codeigniter.com/docs>`_
-  `Language File Translations <https://github.com/bcit-ci/codeigniter3-translations>`_
-  `Community Forums <http://forum.codeigniter.com/>`_
-  `Community Wiki <https://github.com/bcit-ci/CodeIgniter/wiki>`_
-  `Community Slack Channel <https://codeigniterchat.slack.com>`_

Report security issues to our `Security Panel <mailto:security@codeigniter.com>`_
or via our `page on HackerOne <https://hackerone.com/codeigniter>`_, thank you.

***************
Acknowledgement
***************

The CodeIgniter team would like to thank EllisLab, all the
contributors to the CodeIgniter project and you, the CodeIgniter user.
