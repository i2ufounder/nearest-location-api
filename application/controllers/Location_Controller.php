<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Location_Controller extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
    }


    public function index_get()
    {
        $origin = new Point();
        // $origin->latitude = 38.98377;
        // $origin->longitude = -94.45931;
        $origin->latitude = $this->input->get('latitude') ? $this->input->get('latitude') : 0;
        $origin->longitude = $this->input->get('longitude') ? $this->input->get('longitude') : 0;

        $service = new Location_Data_Service();
        $locations = $service->search_for_locations($origin, 25, 100);

        foreach ($locations as $location) {
            $km = $location->calc_distance_to($origin);
            $location->distance = $location::km_to_miles($km);
        }

        usort($locations, 'Location_Controller::compare_distance');

        $this->response($locations, REST_Controller::HTTP_OK);
    }

    private static function compare_distance($a, $b)
    {
        if ($a->distance == $b->distance) {
            return 0;
        }
        return $a->distance > $b->distance;
    }

}