<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Location extends CI_Model
{
    public $id = 0;
    public $name = "";
    public $address = "";
    public $city = "";
    public $zip = "";
    public $latitude = 0.0;
    public $longitude = 0.0;

    const kmToMile = 0.62137119;

    // Earth's circumference in km
    const radius = 6371;
    const circumference = 2 * M_PI * self::radius;
    const kmPerDeg = self::circumference / 360;

    public function __construct()
    {
    }


    /**
     * Calculates distance from this location to the provided point
     *
     * @param $origin(Point)
     * @return double
     */
    public function calc_distance_to(Point $origin)
    {
        $dLat = deg2rad($origin->latitude - $this->latitude);
        $dLon = deg2rad($origin->longitude - $this->longitude);
        $lat1 = deg2rad($this->latitude);
        $lat2 = deg2rad($origin->latitude);

        $arc = sin($dLat / 2) * sin($dLat / 2) + sin($dLon / 2) * sin($dLon / 2) * cos($lat1) * cos($lat2);

        $c = 2 * atan2(sqrt($arc), sqrt(1 - $arc));

        return (double)self::radius * $c;
    }


    /**
     * Converts km to miles
     *
     * @param $km - kilometers
     * @return miles
     */
    public static function km_to_miles($km)
    {
        return (double)self::kmToMile * $km;
    }

}