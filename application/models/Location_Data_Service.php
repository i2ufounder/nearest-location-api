<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Location_Data_Service extends CI_Model
{
    public function __construct()
    {
    }

    /**
     * Retrieves all locations in DB
     * TODO: Extend this to handle paging
     *
     * @return array of Locations
     */
    public function get_all_locations()
    {
        $query = $this->db->query('SELECT * FROM locations');
        $rows = $query->custom_result_object('Location');
        return $rows;
    }

    /**
     * Retrieves all location in DB where the latitude and longitude
     * are within the ranges provided
     *
     * @return array of Locations
     */
    private function get_locations_in_long_lat_range( $minLat, $maxLat, $minLong, $maxLong )
    {
        $sql = 'SELECT * FROM locations WHERE latitude > ? AND latitude < ? AND longitude > ? AND longitude < ?';
        $query = $this->db->query($sql, [$minLat, $maxLat, $minLong, $maxLong]);
        $rows = $query->custom_result_object('Location');
        return $rows;
    }


    /**
     * Simple algorithm to hunt for locations
     * within distance of point provided until we have
     * no more than $max_locations requested
     *
     * @param $origin - the point we are trying to get locations for
     * @param $dist - the starting distance we are looking to find the locations within
     * @param $max_locations - the maximum number of locations we want in our search
     * @return array of Locations
     */
    public function search_for_locations(Point &$origin, $dist, $max_locations)
    {
        $latRange = $origin->get_latitude_range_km($dist);
        $longRange = $origin->get_longitude_range_km($dist, $origin->latitude);

        $locations = $this->get_locations_in_long_lat_range($latRange['min'], $latRange['max'], $longRange['min'], $longRange['max']);

        $count_locations = count($locations);

        // If we didn't get any locations, widen the search
        if ($count_locations == 0) {
            $dist = $dist * 2;
            return $this->search_for_locations($origin, $dist, $max_locations);
        }

        // If we got more than $max_locations locations, narrow the search
        if ($count_locations > $max_locations) {
            $dist = $dist / 1.3;
            return $this->search_for_locations($origin, $dist, $max_locations);
        }

        return $locations;
    }
}