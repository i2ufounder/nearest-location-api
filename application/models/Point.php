<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Point extends CI_Model
{
    public $latitude = 0.0;
    public $longitude = 0.0;

    // Earth's circumference in km
    const radius = 6371;
    const circumference = 2 * M_PI * self::radius;
    const kmPerDeg = self::circumference / 360;

    public function __construct($lat = 0, $long = 0)
    {
        $this->latitude = $this->check_bounds_of_latitude($lat);
        $this->longitude = $this->check_bounds_of_longitude($long);
    }


    /**
     * Keep latitude within sane values
     *
     * @param $input
     * @return bounded $input between -90 and +90
     */
    private function check_bounds_of_latitude($input)
    {
        $abs = abs($input);
        if ($abs > 90) {
            $sign = ($input > 0) - ($input < 0);
            return 90 * $sign;
        }
        return $input;
    }

    /**
     * Keep longitude within sane values
     *
     * @param $input
     * @return bounded $input between -180 and +180
     */
    private function check_bounds_of_longitude($input)
    {
        $abs = abs($input);
        if ($abs > 180) {
            $sign = ($input > 0) - ($input < 0);
            return 180 * $sign;
        }
        return $input;
    }


    /**
     * Simple calc to get min/max latitude range
     * around point
     *
     * @param $km
     * @return array of minimum and maximum
     */
    public function get_latitude_range_km($km)
    {
        $min = $this->latitude - ($km / 2 / self::kmPerDeg);
        $min = $this->check_bounds_of_latitude($min);
        $max = $this->latitude + ($km / 2 / self::kmPerDeg);
        $max = $this->check_bounds_of_latitude($max);

        return [
            'min' => $min,
            'max' => $max
        ];
    }


    /**
     * Simple calc to get min/max longitude range
     * around point
     *
     * @param $km
     * @param $lat
     * @return array of minimum and maximum
     */
    public function get_longitude_range_km($km, $lat)
    {
        $min = $this->longitude - ($km / 2 / cos(deg2rad($lat)));
        $min = $this->check_bounds_of_longitude($min);
        $max = $this->longitude + ($km / 2 / cos(deg2rad($lat)));
        $max = $this->check_bounds_of_longitude($max);

        return [
            'min' => $min,
            'max' => $max
        ];
    }

}