<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Init
{
    private $CI;
    private $dbName;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->dbforge();
        $this->CI->load->dbutil();
        $this->dbName = $this->CI->db->database;
        $this->init();
    }

    public function init()
    {
        // Make sure database exists
        if (!$this->CI->dbutil->database_exists($this->dbName)) {
            $this->CI->dbforge->create_database($this->dbName);
        }

        // Make sure tables exist
        if (!$this->CI->db->table_exists('locations')) {
            $this->CI->dbforge
                ->add_field('id')
                ->add_field('`name` VARCHAR(100) NOT NULL DEFAULT ""')
                ->add_field('`address` longtext NOT NULL DEFAULT ""')
                ->add_field('`city` VARCHAR(100) NOT NULL DEFAULT ""')
                ->add_field('`state` VARCHAR(20) NOT NULL DEFAULT ""')
                ->add_field('`zip` VARCHAR(5) NOT NULL DEFAULT ""')
                ->add_field('`latitude` DECIMAL(10,5) NOT NULL DEFAULT 0.0')
                ->add_field('`longitude`  DECIMAL(10,5) NOT NULL DEFAULT 0.0')
                ->add_key('latitude')
                ->add_key('longitude')
                ->create_table('locations');
        }



    }
}